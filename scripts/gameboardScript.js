var time=120000;

	function StopInterval(intID){
		clearInterval(intID);
	
	}

	var intID;
	function displayTimer() {
    	intID= setInterval(function(){

    				
    		var minutes=Math.floor(time% (1000 * 60 * 60) / (1000 * 60));
    		var seconds=time % (1000 * 60) / 1000;
    		if (seconds<10) {
    			document.getElementById("timer").innerHTML = minutes+":0"+seconds;
    		}else{
    			document.getElementById("timer").innerHTML = minutes+":"+seconds;
    		}
    		time=time-1000;
					

                    
			//change hourglass image at certain times -Ali
		if (minutes==1){
			if (seconds==50){
				document.getElementById("hourglass").src="images/0.10.png";
			}
			if (seconds==40){
				document.getElementById("hourglass").src="images/0.20.png";
			}
			if (seconds==30){
				document.getElementById("hourglass").src="images/0.30.png";
			}
			if (seconds==20){
				document.getElementById("hourglass").src="images/0.40.png";
			}
			if (seconds==10){
				document.getElementById("hourglass").src="images/0.50.png";
			}
			if (seconds==0){
				document.getElementById("hourglass").src="images/1.00.png";
			}
		}
		if (minutes==0){
			
			if (seconds==50){
				document.getElementById("hourglass").src="images/1.10.png";
			}
			if (seconds==40){
				document.getElementById("hourglass").src="images/1.20.png";
			}
			if (seconds==30){
				document.getElementById("hourglass").src="images/1.30.png";
			}
			if (seconds==20){
				document.getElementById("hourglass").src="images/1.40.png";
			}
			if (seconds==10){
				document.getElementById("hourglass").src="images/1.50.png";
			}
		}
			if (minutes<=0 && seconds<=0){
				//call function to display summary- Ali
				summary();
				
				//hide game grid and display gameover- Ali
                var grid=document.getElementById("records");
                var instructions=document.getElementById("instructions");
                grid.style.display='none';
                instructions.style.display='none';
				document.getElementById("timer").innerHTML= "Gameover";
				document.getElementById("EndTime").innerHTML="00:00";
				StopInterval(intID);
				
				
				
				
				
				
				
            }
		}, 1000);
		
	}

//<!--enlarge pic here-->

		function enlargeFunction(photo){

            //<!--get model id-->
            var modal = document.getElementById('Enlarge');

            //<!--get the image and put it inside the model and use alt as info-->
            
            var PhotoImg = document.getElementById("img1");
            var infoTitle=document.getElementById("heading")
            var infoText = document.getElementById("info");
                
                modal.style.display = "block";
                PhotoImg.src = photo.src;
                infoTitle.innerHTML=photo.title;
                infoText.innerHTML = photo.alt;



            //<!--span function by class-->
            var span = document.getElementsByClassName("close")[0];

            //<!--close modal function-->
            span.onclick = function() {
                modal.style.display = "none";
            }
            }

	//add score function
	function addScore(){
		
		currentScore = document.getElementById("score").innerHTML;
		currentScore = parseInt(currentScore)+50;
		document.getElementById("score").innerHTML=currentScore;
	}
	
			
			
			

    //match and remove function
	
    var pic1 = null;
    var pic2 = null;
    var left_pics = null;
	var map={
		record1:'record2',
		record3:'record4',
		record5:'record6',
		record7:'record8'}
	
    function remove(obj){
        var temp = obj.id;
		//if no other image is selected, let this image become picture1
        if(pic1==null){
            pic1 = temp;
        }
		//if another image has been selected (picture1 is not null)
        else{
			//if second image is the exact same as the first, deselect- ie. same image selected twice
            if(pic1==temp){
                pic1 = null;
            }
			//if the photos have matching topics (change src to topic attr), remove them
            else if(map[pic1]==temp||map[temp]==pic1){
                pic2 = temp;
                $('#'+pic1).css('opacity', '0')
                $('#'+pic2).css('opacity', '0')
                left_pics-=2;
          		pic1 = null;
          		pic1 = null;
				addScore();
            }
            else{
                pic1=null;
                time-=10000;
                alert("-10 seconds")
            }
        }
    }
    function matchRemove(obj){
    	if(left_pics==null)
    		left_pics = $('.record').length;
    	remove(obj);
    	checkGame();
    }
    
    function checkGame(){
    	
    	if(left_pics==0){
    		summary();
    		//hide game grid and display gameover- Ali
        	var grid=document.getElementById("records");
       	 	var instructions=document.getElementById("instructions");
        	grid.style.display='none';
        	instructions.style.display='none';
			document.getElementById("timer").innerHTML= "Gameover";
				
			StopInterval(intID);
    	}
    	
    }

    
    function summary(){
		clearInterval();
        
		console.log("It works!");
		document.getElementById("hourglass").src="images/2.00.png";
		var minutes=Math.floor(time% (1000 * 60 * 60) / (1000 * 60));
    	var seconds=time % (1000 * 60) / 1000;
    	if (seconds<10) {
    			document.getElementById("EndTime").innerHTML = minutes+":0"+seconds;
    		}else{
    			document.getElementById("EndTime").innerHTML = minutes+":"+seconds;
    	}
		document.getElementById("Summary").style.display="block";
        document.getElementById("Summary").style.visibility="visible";
        document.getElementById("EndPoints").innerHTML=document.getElementById("score").innerHTML;

        document.getElementById("Total").innerHTML=document.getElementById("score").innerHTML;
			
			
    }
	
	function submitScore(){
		document.getElementById("Summary").style.visibility="hidden";
		document.getElementById("Summary").style.display="none";
	
		document.getElementById("FORM").style.display="block";
		document.getElementById("FORM").style.visibility="visible";
	
		var score= document.getElementById("Total").innerHTML;
		var time= document.getElementById("EndTime").innerHTML;
		
		document.getElementById("scoreData").value=score;
		document.getElementById("timeData").value=time;
	
	}
